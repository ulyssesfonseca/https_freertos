@echo off
set PROJETO="COPA/SD_CARD"
set REPO="PROJECT-SD"

for /f %%O in ('svn_revision_return') do (set VAR=%%O)
set /a REV=%VAR%+1
echo %VAR%
echo %REV%

For /f "tokens=1-4 delims=/ " %%a in ('date /t') do (set mydate=%%c%%b%%a)

Set FOLDER="..\Project Output\%PROJETO%"
echo %FOLDER%
cd %FOLDER%
Set FILE="%REPO%-R%REV%-%mydate%.zip"

"c:\Program Files\7-Zip\7z.exe" a %FILE% "RECUSER"
"c:\Program Files\7-Zip\7z.exe" a %FILE% "SYSTEM"
"c:\Program Files\7-Zip\7z.exe" a %FILE% "UPDATE"

start .