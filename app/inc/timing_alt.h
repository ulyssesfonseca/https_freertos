

#ifndef __TIMING_ALT__
#define __TIMING_ALT__

#include <stdint.h>


/**
 * \brief          timer structure
 */
struct mbedtls_timing_hr_time
{
    unsigned char opaque[32];
};

/**
 * \brief          Context for mbedtls_timing_set/get_delay()
 */
typedef struct mbedtls_timing_delay_context
{
    struct mbedtls_timing_hr_time   timer;
    uint32_t                        int_ms;
    uint32_t                        fin_ms;
} mbedtls_timing_delay_context;




unsigned long mbedtls_timing_hardclock( void );

unsigned long mbedtls_timing_get_timer( struct mbedtls_timing_hr_time *val, int reset );

void mbedtls_set_alarm( int seconds );

void mbedtls_timing_set_delay( void *data, uint32_t int_ms, uint32_t fin_ms );

int mbedtls_timing_get_delay( void *data );

int mbedtls_timing_self_test( int verbose );

#endif
