

#include "timing_alt.h"



volatile int mbedtls_timing_alarmed;

unsigned long mbedtls_timing_hardclock( void ){}

unsigned long mbedtls_timing_get_timer( struct mbedtls_timing_hr_time *val, int reset ){}

void mbedtls_set_alarm( int seconds ){}

void mbedtls_timing_set_delay( void *data, uint32_t int_ms, uint32_t fin_ms ){}

int mbedtls_timing_get_delay( void *data ){}

int mbedtls_timing_self_test( int verbose ){}
